node[:deploy].each do |application, deploy|

  if deploy[:application] != 'b2bstore'
    next
  end

  script "install_dependencies" do
    interpreter "bash"
    user "root"
    cwd "#{deploy[:deploy_to]}/current"
    code <<-EOH
    php5enmod mcrypt
    service apache2 restart

    php composer.phar install --no-dev --no-interaction --optimize-autoloader

    echo "Install modman"
    bash < <(wget -q --no-check-certificate -O - https://raw.github.com/colinmollenhour/modman/master/modman-installer)
    sudo ln -s /root/bin/modman /bin
    chmod +x /bin/modman

    EOH
  end

  deploy[:environment_variables].each do |key, value|
    script "set_envs" do
      interpreter "bash"
      user "root"
      cwd "#{deploy[:deploy_to]}/current"
      code <<-EOH
      sed '/^\s*$/d' /etc/environment > /etc/environment.back
      mv /etc/environment.back /etc/environment
      sed -i  -E 's/#{key}=(.*?)//g' /etc/environment
      export #{key}=#{value}
      echo "" >> /etc/environment
      echo #{key}=#{value} >> /etc/environment
      EOH
    end
  end

  script "install" do
      interpreter "bash"
      user "root"
      cwd "#{deploy[:deploy_to]}/current"
      code <<-EOH
      for env in $(cat  /etc/environment); do export $env; done

      php public/install.php -- \
      				--license_agreement_accepted "yes" \
      				--locale "pl_PL" \
      				--timezone "Europe/Warsaw" \
      				--default_currency "PLN" \
      				--use_rewrites "yes" \
      				--use_secure "no" \
      				--secure_base_url "" \
      				--use_secure_admin "no" \
      				--db_host $DB_HOST \
      				--db_name $DB_NAME \
      				--db_user $DB_USER \
      				--db_pass $DB_PASSWORD \
      				--url $URL \
      				--skip_url_validation \
      				--admin_email $ADMIN_EMAIL \
      				--admin_username $ADMIN_USERNAME \
      				--admin_password $ADMIN_PASSWORD \
      				--admin_firstname $ADMIN_FIRST_NAME \
      				--admin_lastname $ADMIN_LAST_NAME
      EOH
  end

  script "deploy_modules" do
    interpreter "bash"
    user "root"
    cwd "#{deploy[:deploy_to]}/current"
    code <<-EOH
    for env in $(cat  /etc/environment); do export $env; done

    echo "Copy local modules";
    cp -R modules/* .modman

    echo "Deploy modules";
    modman deploy-all --copy --force
    EOH
  end

    script "set_chmods" do
      interpreter "bash"
      user "root"
      cwd "#{deploy[:deploy_to]}/current"
      code <<-EOH
      sudo chown -R www-data:www-data .
      find data/ -type d -exec chmod 700 {} \;
      find public/var/ -type f -exec chmod 600 {} \;
      find public/media/ -type f -exec chmod 600 {} \;
      find public/var/ -type d -exec chmod 700 {} \;
      find public/media/ -type d -exec chmod 700 {} \;
      chmod 700 public/includes
      chmod 600 public/includes/config.php
      EOH
    end

end