template "/etc/log_files.yml" do
  source 'log_files.erb'
  action :create
  mode '0755'
  owner 'root'
  group 'root'
end

script "install_papertrail" do
  interpreter "bash"
  user "root"
  code <<-EOH
  echo "*.*          @logs2.papertrailapp.com:54265" >> /etc/rsyslog.conf
  wget https://github.com/papertrail/remote_syslog2/releases/download/v0.14-beta-pkgs/remote-syslog2_0.14_amd64.deb
  dpkg -i remote-syslog2_0.14_amd64.deb
  sudo service remote_syslog restart
  EOH
end
