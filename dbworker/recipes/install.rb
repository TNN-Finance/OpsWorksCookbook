include_recipe 'deploy'

node[:deploy].each do |application, deploy|
  if deploy[:application] != 'quotesdbworker'
    next
  end

  opsworks_deploy_dir do
    user deploy[:user]
    group deploy[:group]
    path deploy[:deploy_to]
  end

  opsworks_deploy do
    deploy_data deploy
    app application
  end

  script "pre_set_envs" do
    interpreter "bash"
    user "root"
    cwd "#{deploy[:deploy_to]}/current"
    code <<-EOH
    echo "" >> /etc/environment
    EOH
  end

  ENVIROMENTSTR = ''
  deploy[:environment_variables].each do |key, value|
    ENVIROMENTSTR = ENVIROMENTSTR + "#{key}='#{value}',"
    script "set_envs" do
      interpreter "bash"
      user "root"
      cwd "#{deploy[:deploy_to]}/current"
      code <<-EOH
      sed '/^\s*$/d' /etc/environment > /etc/environment.back
      mv /etc/environment.back /etc/environment
      sed -i  -E 's/#{key}=(.*?)//g' /etc/environment
      export #{key}=#{value}
      echo "" >> /etc/environment
      echo #{key}=#{value} >> /etc/environment
      EOH
    end
  end
  ENVIROMENTSTR = ENVIROMENTSTR.chomp(',')



  script "install_composer_and_prepare_for_daemon" do
    interpreter "bash"
    user "root"
    cwd "#{deploy[:deploy_to]}/current"
    code <<-EOH
    curl -s https://getcomposer.org/installer | php
    php composer.phar install --no-dev --no-interaction --optimize-autoloader
    for env in $(cat  /etc/environment); do export $env; done
    EOH
  end

  template "/etc/supervisor/conf.d/dbwoker.conf" do
    source 'dbwoker.erb'
    action :create
    mode '0755'
    owner 'root'
    group 'root'
    variables({
                  :ENV => ENVIROMENTSTR
              })
  end

  execute "restart supervisor" do
    command "service supervisor restart"
    user "root"
  end
end