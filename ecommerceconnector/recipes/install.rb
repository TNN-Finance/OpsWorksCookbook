node[:deploy].each do |application, deploy|

  if deploy[:application] != 'ecommerceconnector'
    next
  end

  script "install_dependencies" do
    interpreter "bash"
    user "root"
    cwd "#{deploy[:deploy_to]}/current"
    code <<-EOH
    php composer.phar install --no-dev --no-interaction --optimize-autoloader
    EOH
  end

  script "create_config" do
      interpreter "bash"
      user "root"
      cwd "#{deploy[:deploy_to]}/current"
      code <<-EOH
      cp config/autoload/local.php.dist config/autoload/local.php
      EOH
  end

  deploy[:environment_variables].each do |key, value|
    script "set_envs" do
      interpreter "bash"
      user "root"
      cwd "#{deploy[:deploy_to]}/current"
      code <<-EOH
      sed '/^\s*$/d' /etc/environment > /etc/environment.back
      mv /etc/environment.back /etc/environment
      sed -i  -E 's/#{key}=(.*?)//g' /etc/environment
      export #{key}=#{value}
      echo "" >> /etc/environment
      echo #{key}=#{value} >> /etc/environment
      EOH
    end
  end


  script "migrate_database" do
    interpreter "bash"
    user "root"
    cwd "#{deploy[:deploy_to]}/current"
    code <<-EOH
    for env in $(cat  /etc/environment); do export $env; done
    php public/index.php migration apply
    EOH
  end



  ENVIROMENTSTR = ''
  deploy[:environment_variables].each do |key, value|
    ENVIROMENTSTR = ENVIROMENTSTR + "#{key}='#{value}',"
    script "set_envs" do
      interpreter "bash"
      user "root"
      cwd "#{deploy[:deploy_to]}/current"
      code <<-EOH
        sed '/^\s*$/d' /etc/environment > /etc/environment.back
        mv /etc/environment.back /etc/environment
        sed -i  -E 's/#{key}=(.*?)//g' /etc/environment
        export #{key}=#{value}
        echo "" >> /etc/environment
        echo #{key}=#{value} >> /etc/environment
      EOH
    end
  end
  ENVIROMENTSTR = ENVIROMENTSTR.chomp(',')


  

end