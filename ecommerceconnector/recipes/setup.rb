node[:deploy].each do |application, deploy|

  if deploy[:application] != 'ecommerceconnector'
    next
  end

  script "prepare_station" do
    interpreter "bash"
    user "root"
    cwd "#{deploy[:deploy_to]}/current/src"
    code <<-EOH

    ## PHP 7
    sudo echo "deb http://repos.zend.com/zend-server/early-access/php7/repos ubuntu/" >> /etc/apt/sources.list
    sudo apt-get update && apt-get install -y --force-yes php7-nightly

    sudo cp /usr/local/php7/libphp7.so /usr/lib/apache2/modules/
    sudo cp /usr/local/php7/php7.load /etc/apache2/mods-available/

    echo "" >>/etc/apache2/apache2.conf
    echo "<FilesMatch \.php$>" >>/etc/apache2/apache2.conf
    echo "   SetHandler application/x-httpd-php" >>/etc/apache2/apache2.conf
    echo "</FilesMatch>" >>/etc/apache2/apache2.conf

    sudo a2dismod mpm_event
    sudo a2enmod mpm_prefork
    sudo a2enmod php7

    sudo ln -s /usr/local/php7/bin/php php

    service apache2 restart
    rm -r ./data/cache
    mkdir -p ./data/cache
    chmod 777 -R ./data
    EOH
  end

end