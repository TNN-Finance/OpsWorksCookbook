node[:deploy].each do |application, deploy|

  if deploy[:application] != 'quotes'
    next
  end

  script "prepare_station" do
    interpreter "bash"
    user "root"
    cwd "#{deploy[:deploy_to]}/current/src"
    code <<-EOH
    printf "\n" | sudo pecl install -Z zmq-beta
    rm /etc/php5/mods-available/zmq.ini
    touch /etc/php5/mods-available/zmq.ini
    echo "extension=zmq.so" > /etc/php5/mods-available/zmq.ini
    php5dismod zmq
    php5enmod zmq
    pecl install zendopcache-7.0.5
    php5dismod opcache
    rm /etc/php5/mods-available/opcache.ini
    touch /etc/php5/mods-available/opcache.ini
    echo "zend_extension=opcache.so" >> /etc/php5/mods-available/opcache.ini
    echo "opcache.revalidate_freq=0" >> /etc/php5/mods-available/opcache.ini
    echo "opcache.validate_timestamps=0" >> /etc/php5/mods-available/opcache.ini
    echo "opcache.max_accelerated_files=7963" >> /etc/php5/mods-available/opcache.ini
    echo "opcache.memory_consumption=1024" >> /etc/php5/mods-available/opcache.ini
    echo "opcache.interned_strings_buffer=16" >> /etc/php5/mods-available/opcache.ini
    echo "opcache.fast_shutdown=1" >> /etc/php5/mods-available/opcache.ini
    php5enmod opcache
    service apache2 restart
    rm -r ./data/cache
    mkdir -p ./data/cache
    chmod 777 -R ./data
    EOH
  end

  script "install_composer" do
    interpreter "bash"
    user "root"
    cwd "#{deploy[:deploy_to]}/current/src"
    code <<-EOH
    curl -s https://getcomposer.org/installer | php
    php composer.phar install --no-dev --no-interaction --optimize-autoloader
    EOH
  end


  deploy[:environment_variables].each do |key, value|
    script "set_envs" do
      interpreter "bash"
      user "root"
      cwd "#{deploy[:deploy_to]}/current/src"
      code <<-EOH
      sed '/^\s*$/d' /etc/environment > /etc/environment.back
      mv /etc/environment.back /etc/environment
      sed -i  -E 's/#{key}=(.*?)//g' /etc/environment
      export #{key}=#{value}
      echo "" >> /etc/environment
      echo #{key}=#{value} >> /etc/environment
      EOH
    end
  end

  script "run_daemon" do
    interpreter "bash"
    user "root"
    cwd "#{deploy[:deploy_to]}/current/src"
    code <<-EOH
    for env in $(cat  /etc/environment); do export $env; done

    echo "Killing process on port #{deploy[:environment_variables][:ZERO_MQ_PORT]}"
    echo "Killing process on port #{deploy[:environment_variables][:ZERO_MQ_MAIN_PORT]}"
    echo "Killing process on port 2015"

    kill -9 $(lsof -i:#{deploy[:environment_variables][:ZERO_MQ_PORT]} -t) 2> /dev/null
    kill -9 $(lsof -i:#{deploy[:environment_variables][:ZERO_MQ_MAIN_PORT]} -t) 2> /dev/null
    kill -9 $(lsof -i:2015 -t) 2> /dev/null
    #curl -H "Host: #{deploy[:domains][0]}" -H "Content-Type: application/vnd.api.v1+json" "http://localhost/quote?EDPSUPERLUMINAL_CACHE&sort=DESC"

    $(php public/index.php daemon stop)

    x=1
    output=$(php public/index.php daemon start)
    status=$?
    echo $status

    while [ $status -ne 0 -a $x -le 5 ]; do
        echo "Fail"
        sleep 5
        output=$(php public/index.php daemon start)
        status=$?
        x=$(( $x + 1 ))
    done

    if [ $status -ne 0 ]; then
        exit -1
    fi

    EOH
  end

end